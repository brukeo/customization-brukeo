<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 * @copyright 2025
 */

namespace Brukeo\Customization\Helper;

class GetCompaniesInCityUrl
{

    public function __construct(
        protected \Magento\Framework\UrlInterface $url
    ) {}

    public function execute(): string
    {
        return str_replace('gdzie-kupic', 'firmy-w-miescie', $this->url->getCurrentUrl());
    }

}
