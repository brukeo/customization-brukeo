<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\Customization\Helper;

class GetProductPageUrl
{

    protected \Magento\Framework\UrlInterface $url;
    protected \Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollectionFactory $urlRewriteCollectionFactory;

    public function __construct(
        \Magento\Framework\UrlInterface $url,
        \Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollectionFactory $urlRewriteCollectionFactory
    )
    {
        $this->url = $url;
        $this->urlRewriteCollectionFactory = $urlRewriteCollectionFactory;
    }

    public function execute(): ?string
    {
        $currentUrl = $this->url->getCurrentUrl();
        $currentUrlChunks = explode('-', $currentUrl);
        $filerVar = end($currentUrlChunks);
        $productId = (int) filter_var($filerVar, FILTER_SANITIZE_NUMBER_INT);
        $targetPath = sprintf("catalog/product/view/id/%s", $productId);

        /** @var \Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollection $urlRewriteCollection */
        $urlRewriteCollection = $this->urlRewriteCollectionFactory->create();
        $urlRewriteCollection->addFieldToFilter(
            \Magento\UrlRewrite\Service\V1\Data\UrlRewrite::TARGET_PATH,
            $targetPath
        );

        if (!$urlRewriteCollection->count()) {
            return null;
        }

        $baseUrl = $this->url->getBaseUrl();
        $requestPath =  (string) $urlRewriteCollection->getFirstItem()->getData(
            \Magento\UrlRewrite\Service\V1\Data\UrlRewrite::REQUEST_PATH
        );

        return sprintf("%s%s", $baseUrl, $requestPath);
    }

}
