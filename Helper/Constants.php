<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\Customization\Helper;

class Constants
{

    const ASSIGN_OLD_BRUKEO_PRODUCT_RELATIONS_FROM_CSV_COMMAND_NAME = 'brukeo:products:assign_old_brukeo_product_relations_from_csv';
    const ASSIGN_OLD_BRUKEO_PRODUCT_RELATIONS_FROM_CSV_COMMAND_DESCRIPTION = 'Assign old brukeo product relations from csv.';

}
