<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\Customization\Helper;

class GetOldBrukeoProductRelationsFromCsv
{

    protected \Magento\Framework\DataObjectFactory $dataObjectFactory;

    public function __construct(\Magento\Framework\DataObjectFactory $dataObjectFactory)
    {
        $this->dataObjectFactory = $dataObjectFactory;
    }

    public function execute(): array
    {
        $file = __DIR__ . '/../_resources/old_brukeo_products_relation.csv';
        $rows = [];
        $handle = fopen($file, "r");
        $i = 1;
        while (($row = fgetcsv($handle)) !== false) {
            if ($i != 1) {
                $rows[$row[0]][] = $row[1];
            }
            $i++;
        }
        fclose($handle);

        return $rows;
    }

}
