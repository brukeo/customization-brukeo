<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\Customization\Console\Command;

class AssignOldBrukeoProductRelationsFromCsv extends \Symfony\Component\Console\Command\Command
{

    protected \Magento\Framework\App\State $appState;
    protected \Brukeo\Customization\Helper\GetOldBrukeoProductRelationsFromCsv $getRelationsFromCsv;
    protected \Brukeo\Customization\Mapper\ProductRelation $productRelationMapper;
    protected \Brukeo\Customization\Model\AssignRelatedProductsBySku $assignRelatedProductsBySku;

    public function __construct
    (
        \Magento\Framework\App\State $appState,
        \Brukeo\Customization\Helper\GetOldBrukeoProductRelationsFromCsv $getRelationsFromCsv,
        \Brukeo\Customization\Mapper\ProductRelation $productRelationMapper,
        \Brukeo\Customization\Model\AssignRelatedProductsBySku $assignRelatedProductsBySku,
        string $name = null
    )
    {
        $this->appState = $appState;
        $this->getRelationsFromCsv = $getRelationsFromCsv;
        $this->productRelationMapper = $productRelationMapper;
        $this->assignRelatedProductsBySku = $assignRelatedProductsBySku;

        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this->setName(\Brukeo\Customization\Helper\Constants::ASSIGN_OLD_BRUKEO_PRODUCT_RELATIONS_FROM_CSV_COMMAND_NAME);
        $this->setDescription(\Brukeo\Customization\Helper\Constants::ASSIGN_OLD_BRUKEO_PRODUCT_RELATIONS_FROM_CSV_COMMAND_DESCRIPTION);

        parent::configure();
    }

    protected function execute(
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output
    ): int
    {
        $this->appState->setAreaCode('frontend');

        $skusWithError = [];
        $relationsFromCsv = $this->getRelationsFromCsv->execute();
        foreach ($relationsFromCsv as $sku => $skusToLink) {
            $output->writeln(sprintf("<info>Processing product: %s.</info>\n<info>Assigning related skus: %s</info>", $sku, implode(', ', $skusToLink)));
            try {
                $productRelation = $this->productRelationMapper->execute($sku, $skusToLink);
                $this->assignRelatedProductsBySku->execute($productRelation);
            } catch (\Exception $exception) {
                $output->writeln(sprintf("<error>%s:</error> %s", $sku, $exception->getMessage()));
                $skusWithError[] = $sku;
            }
        }

        $skusWithError = implode(", ", $skusWithError);
        $output->writeln(sprintf("<error>SKU's with error: %s</error>", $skusWithError));

        return 1;
    }

}
