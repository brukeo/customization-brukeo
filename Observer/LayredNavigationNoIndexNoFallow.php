<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 * @copyright 2024 Marceli Podstawski
 */

namespace Brukeo\Customization\Observer;

class LayredNavigationNoIndexNoFallow implements \Magento\Framework\Event\ObserverInterface
{

    protected \Magento\Framework\App\Request\Http $request;
    protected \Magento\Framework\View\Page\Config $layoutFactory;

    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\View\Page\Config $layoutFactory
    )
    {
        $this->request = $request;
        $this->layoutFactory = $layoutFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->request->getControllerName() != 'category') {
            return;
        }

        if (count($this->request->getParams()) == 1) {
            return;
        }

        $this->layoutFactory->setRobots('NOINDEX,FOLLOW');
    }

}
