<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\Customization\Dto;

class RelatedProducts
{

    /** @var \Brukeo\Customization\Dto\RelatedProduct[] */
    protected array $items;

    /**
     * @return \Brukeo\Customization\Dto\RelatedProduct[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function addItem(\Brukeo\Customization\Dto\RelatedProduct $relatedProduct): void
    {
        $this->items[] = $relatedProduct;
    }
}
