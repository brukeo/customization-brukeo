<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\Customization\Dto;

class ProductRelation
{

    protected string $sku;
    protected \Brukeo\Customization\Dto\RelatedProducts $relatedProducts;

    public function __construct(
        string $sku,
        \Brukeo\Customization\Dto\RelatedProducts $relatedProducts
    )
    {
        $this->sku = $sku;
        $this->relatedProducts = $relatedProducts;
    }

    public function getSku(): string
    {
        return $this->sku;
    }

    public function getRelatedProducts(): \Brukeo\Customization\Dto\RelatedProducts
    {
        return $this->relatedProducts;
    }

}
