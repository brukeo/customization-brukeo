<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\Customization\Mapper;

class ProductRelation
{

    protected \Brukeo\Customization\Dto\ProductRelationFactory $productRelationFactory;
    protected \Brukeo\Customization\Dto\RelatedProductFactory $relatedProductFactory;
    protected \Brukeo\Customization\Dto\RelatedProductsFactory $relatedProductsFactory;

    public function __construct(
        \Brukeo\Customization\Dto\ProductRelationFactory $productRelationFactory,
        \Brukeo\Customization\Dto\RelatedProductFactory $relatedProductFactory,
        \Brukeo\Customization\Dto\RelatedProductsFactory $relatedProductsFactory
    )
    {
        $this->productRelationFactory = $productRelationFactory;
        $this->relatedProductFactory = $relatedProductFactory;
        $this->relatedProductsFactory = $relatedProductsFactory;
    }

    public function execute(string $sku, array $items): \Brukeo\Customization\Dto\ProductRelation
    {
        /** @var \Brukeo\Customization\Dto\RelatedProducts $relatedProducts */
        $relatedProducts = $this->relatedProductsFactory->create();
        foreach ($items as $item) {
            $relatedProduct = $this->relatedProductFactory->create([
                'sku' => $item
            ]);
            $relatedProducts->addItem($relatedProduct);
        }

        return $this->productRelationFactory->create([
            'sku' => $sku,
            'relatedProducts' => $relatedProducts
        ]);
    }

}
