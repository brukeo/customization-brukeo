<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\Customization\Model;

class AssignRelatedProductsBySku
{

    protected \Magento\Catalog\Api\ProductRepositoryInterface $productRepository;
    protected \Magento\Catalog\Api\Data\ProductLinkInterfaceFactory $productLinkFactory;

    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Api\Data\ProductLinkInterfaceFactory $productLinkFactory
    )
    {
        $this->productRepository = $productRepository;
        $this->productLinkFactory = $productLinkFactory;
    }

    public function execute(\Brukeo\Customization\Dto\ProductRelation $productRelation): void
    {
        $links = [];
        $product = $this->productRepository->get($productRelation->getSku());
        $relatedProducts = $productRelation->getRelatedProducts();
        foreach ($relatedProducts->getItems() as $item) {
            try {
                $relatedProductSku = $item->getSku();
                $relatedProduct = $this->productRepository->get($relatedProductSku);

                $productLink = $this->productLinkFactory->create();
                $productLink->setSku($productRelation->getSku())
                    ->setLinkType('related')
                    ->setLinkedProductSku($relatedProduct->getSku())
                    ->setLinkedProductType($relatedProduct->getTypeId())
                    ->setPosition(0);

                $links[] = $productLink;
            } catch (\Exception $exception) {
                continue;
            }
        }

        if (!empty($links)) {
            $product->setProductLinks($links);
            $this->productRepository->save($product);
        }
    }

}
