<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 * @copyright 2025
 */

namespace Brukeo\Customization\Plugin\Magento\Cms\Controller\Noroute\Index;

class RedirectToCompaniesInCityUrl
{

    public function __construct(
        protected \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        protected \Brukeo\Customization\Helper\GetCompaniesInCityUrl $getCompaniesInCityUrl
    ) {}

    public function aroundExecute(
        \Magento\Cms\Controller\Noroute\Index $subject,
        \Closure $proceed
    ): \Magento\Framework\Controller\ResultInterface
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setHttpResponseCode(301);
        $resultRedirect->setUrl($this->getCompaniesInCityUrl->execute());

        return $resultRedirect;
    }

}
