<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\Customization\Plugin\Magento\Cms\Controller\Noroute\Index;

class RedirectToProductPageUrl
{

    protected \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory;
    protected \Brukeo\Customization\Helper\GetProductPageUrl $getProductPageUrl;

    public function __construct(
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        \Brukeo\Customization\Helper\GetProductPageUrl $getProductPageUrl
    )
    {
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->getProductPageUrl = $getProductPageUrl;
    }

    public function aroundExecute(
        \Magento\Cms\Controller\Noroute\Index $subject,
        \Closure $proceed
    ): \Magento\Framework\Controller\ResultInterface
    {
        $productPageUrl = $this->getProductPageUrl->execute();
        if (!$productPageUrl) {
            return $proceed();
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setHttpResponseCode(301);
        $resultRedirect->setUrl($productPageUrl);

        return $resultRedirect;
    }

}
